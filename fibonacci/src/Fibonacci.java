/**
 * Created by spss on 17-4-16.
 */
import java.math.BigInteger;
import java.util.ArrayList;

public class Fibonacci {
    private static int log2N_ceil(BigInteger n){
        int ret = 0;
        BigInteger i = BigInteger.ONE;

        while(i.compareTo(n) < 0){ // i < n
            ret++;
            i = i.multiply(BigInteger.valueOf(2));
        }

        return ret;
    }

    private static BigInteger fib_help(BigInteger n, int expo, ArrayList<BigInteger> f1_seqs, ArrayList<BigInteger> f2_seqs){
        if(n.equals(BigInteger.ZERO)) return BigInteger.ZERO;
        if(n.equals(BigInteger.ONE)) return BigInteger.ONE;
        if(n.equals(BigInteger.valueOf(2))) return BigInteger.ONE;

        BigInteger new_n = n.add( BigInteger.valueOf(2).pow(expo).negate() ); //
        int new_expo = log2N_ceil(new_n) - 1;
        BigInteger new_n2 = new_n.add(BigInteger.ONE);
        int new_expo2 = log2N_ceil(new_n2) - 1;

        BigInteger n1 = f1_seqs.get(expo-1).multiply( fib_help(new_n, new_expo, f1_seqs, f2_seqs) );
        BigInteger n2 = f2_seqs.get(expo-1).multiply( fib_help(new_n2, new_expo2, f1_seqs, f2_seqs) );

        return n1.add(n2);
    }

    public static BigInteger fib(BigInteger n) { // product of factors
        if(n.signum() < 0){
            BigInteger ret = fib(n.negate());

            int R = Math.abs(n.mod(BigInteger.valueOf(2)).intValueExact());
            return R == 0 ? ret.negate() : ret;
        }

        if(n.equals(BigInteger.ZERO)) return BigInteger.ZERO;
        if(n.equals(BigInteger.ONE)) return BigInteger.ONE;
        if(n.equals(BigInteger.valueOf(2))) return BigInteger.ONE;

        int sl = log2N_ceil(n)-1;

        ArrayList<BigInteger> betas = new ArrayList<>();
        ArrayList<BigInteger> f1_seqs = new ArrayList<>(); //f_1, f_3, f_7, ..., f_{2^i-1}
        ArrayList<BigInteger> f2_seqs = new ArrayList<>(); //f_2, f_4, f_8, ..., f_{2^i}

        betas.add(BigInteger.valueOf(3)); // beta_1 = 3
        for(int i=1;i<sl;i++){
            BigInteger beta_last = betas.get(betas.size()-1);
            betas.add(beta_last.pow(2).add(BigInteger.valueOf(-2))); //beta_i = beta_{i-1}^2 - 2
        }

        f1_seqs.add(BigInteger.valueOf(1)); //f_1
        f2_seqs.add(BigInteger.valueOf(1)); //f_2

        for(int i=1;i<sl;i++){ //f_4, f_8, f_16, ..., f_{2^i}
            BigInteger f2_last = f2_seqs.get(f2_seqs.size()-1);
            f2_seqs.add(f2_last.multiply(betas.get(i-1)));
        }

        for(int i=1;i<sl;i++){
            BigInteger f1_last = f1_seqs.get(f1_seqs.size()-1);
            f1_seqs.add(f1_last.multiply(betas.get(i-1)).add(BigInteger.valueOf(-1))); //f_{2^i + k} = f_{2^{i-1}+k} beta_i - f_k
        }

//        System.out.println("sl:" + sl);
//        System.out.println("f1_seq:");
//        for(int i=0;i<f1_seqs.size();i++){
//            System.out.println(f1_seqs.get(i));
//        }
//        System.out.println("f2_seq:");
//        for(int i=0;i<f2_seqs.size();i++){
//            System.out.println(f2_seqs.get(i));
//        }

        return fib_help(n, sl, f1_seqs, f2_seqs);
    }

    // reference: http://ir.library.oregonstate.edu/xmlui/bitstream/handle/1957/39942/HollowayJamesL1989.pdf
    public static void main(String[] args) {
        for(int i=0;i<=20;i++)
            System.out.println(fib(BigInteger.valueOf(i)));
    }
}
