/**
 * Created by spss on 17-4-16.
 */
public class BinOp extends Ast {
    Ast left, right;

    BinOp(String type, Ast left, Ast right) {
        super(type);

        this.left = left;
        this.right = right;
    }

    @Override
    public String toString() {
        String leftStr = left.toString();
        String rightStr = right.toString();

        return String.format("BinOp(%s, %s, %s)", getType(), leftStr, rightStr);
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof BinOp){
            BinOp other = (BinOp)o;

            return getType().equals(other.getType()) &&
                    left.equals(other.left) && right.equals(other.right);
        }

        return false;
    }

    private static int reduce(Ast a){
        if(a instanceof UnOp){
            assert a.getType().equals("imm");

            return ((UnOp) a).getValue();
        }

        int ret = -1;
        BinOp binOp = (BinOp)a;
        switch(binOp.getType()){
            case "+":
            {
                ret = reduce(binOp.left) + reduce(binOp.right);
                break;
            }
            case "-":
            {
                ret = reduce(binOp.left) - reduce(binOp.right);
                break;
            }
            case "*":
            {
                ret = reduce(binOp.left) * reduce(binOp.right);
                break;
            }
            case "/":
            {
                ret = reduce(binOp.left) / reduce(binOp.right);
                break;
            }
            default: //unknown type, this should never happen
            {
                System.err.println("Something must be seriously wrong with the parser");
                break;
            }
        }

        return ret;
    }

    @Override
    public Ast simplify(){
        if(isConstant()){
            return new UnOp("imm",reduce(this));
        }

        return new BinOp(getType(), left.simplify(), right.simplify());
    }

    @Override
    protected boolean isConstant(){
        return left.isConstant() && right.isConstant();
    }
}
