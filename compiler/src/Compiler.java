import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Compiler {
    public List<String> compile(String prog) {
        return pass3(pass2(pass1(prog)));
    }

    /**
     * Returns an un-optimized Ast
     */
    public Ast pass1(String prog) {
        try {
            return Parser.parse(tokenize(prog));
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * Returns an Ast with constant expressions reduced
     */
    public Ast pass2(Ast func) {
        return func.simplify();
    }

    /**
     * Returns assembly instructions
     */
    public List<String> pass3(Ast ast) {
        return CodeGen.generateCode(ast);
    }

    private static Deque<String> tokenize(String prog) {
        Deque<String> tokens = new LinkedList<>();
        Pattern pattern = Pattern.compile("[-+*/()\\[\\]]|[a-zA-Z]+|\\d+");
        Matcher m = pattern.matcher(prog);
        while (m.find()) {
            tokens.add(m.group());
        }
        tokens.add("$"); // end-of-stream
        return tokens;
    }

//    public static void main(String[] args) {
//        String program1 = "[ a b ] a*a + b*b";
//        String program2 = "[ first second ] (first + second) / 2";
//        String program3 = "[ x y ] ( x + y ) / 2";
//        String program4 = "[ x ] x + 2*5";
//        String program5 = "[ ] 2+5*12/10";
//
//
//        Compiler compiler = new Compiler();
//
////        for(String s : Compiler.tokenize(program1)){
////            System.out.println(s);
////        }
////        for(String s : Compiler.tokenize(program2)){
////            System.out.println(s);
////        }
//
//        System.out.println(compiler.pass1(program1));
//        System.out.println(compiler.pass1(program2));
//        System.out.println(compiler.pass1(program3));
//        System.out.println(compiler.pass1(program4));
//
//        System.out.println("==============================================");
//
//        System.out.println(compiler.pass2(compiler.pass1(program4)));
//        System.out.println(compiler.pass2(compiler.pass1(program5)));
//
//        List<String> prog4p3 = compiler.compile(program4);
//        List<String> prog5p3 = compiler.compile(program5);
//
//        System.out.println(Simulator.simulate(prog4p3,15));
//        System.out.println(Simulator.simulate(prog5p3));
//    }
}