import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by spss on 17-4-16.
 */

public class Parser {
    private static void consumeToken(String s, Deque<String> tokens) throws Exception{
        if(!s.equals(tokens.getFirst()))
            throw new Exception(String.format("parsing error, expected %s, got %s",
                    s, tokens.getFirst()));

        tokens.removeFirst();
    }

    private static String peek(Deque<String> tokens){
        return tokens.getFirst();
    }

    private static boolean checkDuplicateArgs(ArrayList<String> args) throws Exception{
        Set<String> s = new HashSet<>(args);
        return s.size() == args.size();
    }

    private static Ast function(Deque<String> tokens) throws Exception{
        consumeToken("[", tokens);

        ArrayList<String> args = new ArrayList<>();
        while(!tokens.peek().equals("]")){
            args.add(tokens.removeFirst());
        }
        if(!checkDuplicateArgs(args)){
            throw new Exception("parsing error, duplicate arguments found");
        }

        consumeToken("]", tokens);

        return expression(tokens,args);
    }

    private static Ast expression(Deque<String> tokens, ArrayList<String> args) throws Exception{
        Ast t = term(tokens,args);

        while(peek(tokens).equals("-") || peek(tokens).equals("+")){
            String op = peek(tokens);
            consumeToken(op,tokens);

            Ast t2 = term(tokens,args); // next term
            t = new BinOp(op, t, t2);
        }

        return t;
    }

    private static Ast term(Deque<String> tokens, ArrayList<String> args) throws Exception{
        Ast t = factor(tokens,args);

        while(peek(tokens).equals("*") || peek(tokens).equals("/")){
            String op = peek(tokens);
            consumeToken(op,tokens);

            Ast t2 = factor(tokens,args); // next term
            t = new BinOp(op, t, t2);
        }

        return t;
    }

    private static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    private static Ast factor(Deque<String> tokens, ArrayList<String> args) throws Exception{
        String token = peek(tokens);
        if(token.equals("(")){
            consumeToken("(",tokens);
            Ast ret = expression(tokens,args);
            consumeToken(")",tokens);

            return ret;
        } else if(isNumeric(token)){
            consumeToken(token, tokens);
            int n = Integer.parseInt(token);

            return new UnOp("imm", n);
        } else if(args.contains(token)) {
            consumeToken(token, tokens);
            int n = args.indexOf(token);

            return new UnOp("arg", n);
        } else {
            throw new Exception("parsing error, unexpected token " + token);
        }
    }

    public static Ast parse(Deque<String> tokens) throws Exception{
        Ast t = function(tokens);
        consumeToken("$",tokens);

        return t;
    }
}
