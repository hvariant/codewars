import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by spss on 17-4-16.
 */
public class CodeGen {
    final private static Map<String, String> opCommand = new HashMap<>();

    static {
        opCommand.put("+","AD");
        opCommand.put("-","SU");
        opCommand.put("*","MU");
        opCommand.put("/","DI");
    }

    private static void genBinOp(List<String> code, BinOp binOp){
        String opcmd = opCommand.get(binOp.getType());

        genCode(code,binOp.left);  // [left] -> R0
        code.add("PU"); // [left] -> stack
        genCode(code,binOp.right); // [right] -> R0
        code.add("SW"); // [right] -> R1
        code.add("PO"); // [left] -> R0
        code.add(opcmd);
    }

    private static void genUnOp(List<String> code, UnOp unOp){
        switch(unOp.getType()){
            case "imm":
            {
                code.add("IM " + unOp.getValue());
                break;
            }
            case "arg":
            {
                code.add("AR " + unOp.getValue());
                break;
            }
            default: //unknown type, this should never happen
            {
                System.err.println("Something must be seriously wrong with the parser");
                break;
            }
        }
    }

    private static void genCode(List<String> code, Ast ast){
        if(ast instanceof UnOp){
            genUnOp(code,(UnOp)ast);
        }
        if(ast instanceof BinOp){
            genBinOp(code,(BinOp)ast);
        }
    }

    public static List<String> generateCode(Ast ast){
        ArrayList<String> code = new ArrayList<>();
        genCode(code,ast);

        return code;
    }
}
