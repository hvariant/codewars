/**
 * Created by spss on 17-4-16.
 */
public class UnOp  extends Ast {
    int n;

    UnOp(String type, int n){
        super(type);

        this.n = n;
    }

    public int getValue(){ return n; }

    @Override
    public String toString(){
        String s = "???";

        switch (getType()){
            case "imm":
            {
                s = "UnOp(imm, " + n + ")";
                break;
            }
            case "arg":
            {
                s = "UnOp(arg, " + n + ")";
                break;
            }
        }

        return s;
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof UnOp){
            UnOp other = (UnOp)o;
            return getType().equals(other.getType()) && n == other.n;
        }

        return false;
    }

    @Override
    public Ast simplify(){
        return new UnOp(this.getType(),this.getValue());
    }

    @Override
    protected boolean isConstant(){
        return getType().equals("imm");
    }
}
