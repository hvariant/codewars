/**
 * Created by spss on 17-4-16.
 */
public abstract class Ast {
    String type;

    Ast(String type){
        this.type = type;
    }

    String getType(){ return type; }

    public abstract String toString();
    public abstract boolean equals(Object o);

    public abstract Ast simplify();
    protected abstract boolean isConstant();
}
